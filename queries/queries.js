const oracledb = require("oracledb");

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
oracledb.autoCommit = true;

const mypw = "hr"; // set mypw to the hr schema password

function connect() {
  return (async function () {
    return await oracledb.getConnection({
      user: "hr",
      password: mypw,
      connectString: "localhost/orcl",
    });
  })();
}

async function countries() {
  let connection = null;
  try {
    connection = await connect();
    const result = await connection.execute(`SELECT * FROM COUNTRIES`);

    return result;
  } catch (err) {
    console.error(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }
}

async function insertAnimals(values) {
  let connection = null;
  try {
    connection = await connect();
    console.log(`INSERT INTO ANIMALS(name, sound) VALUES ${values}`);
    const result = await connection.execute(
      "INSERT INTO ANIMALS(name, sound) VALUES " + values
    );
    return result;
  } catch (err) {
    console.error(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }
}

async function getAnimals() {
  let connection = null;
  try {
    connection = await connect();
    const result = await connection.execute(
      `SELECT * FROM ANIMALS`
    );
    return result;
  } catch (err) {
    console.error(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }
}


module.exports = { countries, insertAnimals, getAnimals };
