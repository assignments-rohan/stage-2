const amqp = require('amqplib/callback_api');

let publishToQueue = async (queue, data, ch) => {
    ch.assertQueue(queue, {
        durable: true
    });
    ch.sendToQueue(queue, Buffer.from(data), {
        persistent: true
    });
    console.log("Sent '%s' to '%s'", data, queue);
};

amqp.connect('amqp://localhost', function (connError, connection) {
    if (connError) {
        throw connError;
    }
    connection.createChannel(function (channelError, channel) {
        if (channelError) {
            throw channelError;
        }
        publishToQueue('jobs', JSON.stringify({ msg: 'Message 1' }), channel);
        channel.close();
        connection.close();
    });
});