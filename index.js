const express = require("express");
const app = express();
const logger = require("./logger/logger");
const { countries, insertAnimals, getAnimals } = require("./queries/queries");
const bodyParser = require("body-parser");
const upload = require("multer")();
const request = require('supertest');

app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", async (req, res) => {
  try {
    let result = await countries();
    res.status(200).send({ data: result.rows });
    logger.info({ get: "/", status: 200 });
  } catch (e) {
    logger.error(e.message);
  }
});

app.get("/animals", async (req, res) => {
  try {
    let result = await getAnimals();
    res.status(200).send(result);
    logger.info({ get: "/animals", status: 200, body: result });
  } catch (e) {
    logger.error(e.message);
  }
});

app.post("/animals", upload.single("textFile"), (req, res) => {
  try {
    let val = req.file.buffer.toString("utf-8").split("\r\n");
    val = val
      .map((el) => {
        let els = el.split(",");
        return `('${els[0]}','${els[1]}')`;
      });

    val.forEach(async v => {
      let result = await insertAnimals(v);
      console.log(result);
    });
  } catch (e) {
    logger.error(e);
  }
});

// Testing Routes
request(app)
  .get('/')
  .expect('Content-Type', /json/)
  .expect(200)
  .end(function (err, res) {
    if (err) throw err;
    console.log('Response: ', res.body);
  });

request(app)
  .get('/animals')
  .expect('Content-Type', /json/)
  .expect(200)
  .end(function (err, res) {
    if (err) throw err;
    console.log('Response: ', res.body);
  });

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on ${port}`));
