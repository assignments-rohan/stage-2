const { createLogger, format, transports } = require('winston');

const logger = createLogger({
    level: 'info',
    format: format.combine(
        format.timestamp(),
        format.json()
    ),
    defaultMeta: { service: 'logger-service' },
    transports: [
        new transports.File({ filename: 'logger-error.log', dirname:'logger/logs', level: 'error' }),
        new transports.File({ filename: 'logger-warn.log', dirname:'logger/logs', level: 'warn' }),
        new transports.File({ filename: 'logger-combined.log',dirname:'logger/logs' }),
        new transports.Console()
    ]
});

module.exports = logger;